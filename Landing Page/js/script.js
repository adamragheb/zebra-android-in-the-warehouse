function thankYscroll() {
  if (document.querySelector(".content").classList.contains("scroll")) {
    //alert("scroll is active");
    document.querySelector('.scroll').scrollIntoView({
      block: 'center',
      behavior: 'smooth'
    });
  }
}

$(document).ready(function () {
  manuliupateDom()
});
$(window).resize(function () {
  manuliupateDom()
});

function manuliupateDom() {
  if ($(this).width() < 768) {
    $('.four-4').insertBefore('.three-3');
    $('.four-4').addClass("mx-3 mb-4");
  } else {
    $('.four-4').insertBefore('.five-5');
    $('.four-4').removeClass("mx-3 mb-4");
  }
}